all: condor

condor:
	mkdir -p condor	
	condor_submit condorOps.cmd

data:
	 cat condor/sim-*.out | grep complexity | sed 's/complexity://' | sed 's/probability://' | sed 's/>>//g' | sed 's/ //g' | sed 's/;/ /g'	> sim-data.txt

clean:
	rm -rf condor
