function processCondorOutput()
    
    %% Get input data from condorData.txt
    fileId = fopen('condorData.txt');
    input = fscanf(fileId, '%f %f');

    complexities = input(1:2:end);
    probabilities = input(2:2:end);

    %% Plots complexities in terms of probabilities
    figure(1)
    clf

    plot(probabilities, complexities, '.');
    xlabel('Rewiring probability p');
    xlim([0.1 0.5]);
    ylabel('Neural complexity');

    drawnow

end

