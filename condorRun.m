%% Script to be run for condor simulation
%% Constructs the network for a randomly selected probability p
%% and calculates its neural complexity

%% initialise random seed according to clock (to obtain different values 
%% for different runs) and get random probability between 0.1 and 0.4
var = mod(sum(10000*clock), 1000);
rand('state',var);
p = 0.1 + 0.4*rand();

%% construct and run network and caculate neural complexity
MF = Q2ConnectRun(p);    
C = Q2NeuralComplexity(MF);

%% print neural complexity and probability
str = sprintf('complexity: %f; probability: %f;', C, p);
disp(str);
