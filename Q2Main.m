function Q2Main

  N = 20; %% number of trials to run

  %% Plot neural complexity against rewiring probability p
  complexities = [];
  probabilities = [];

  %% minimum and maximum values for randomly chosen probability
  minP = 0.1;
  maxP= 0.5;

  for i = 1:N
    %% initialise random seed according to time
    var = mod(sum(10000*clock), 1000);
    rand('state',var);

    %% get random probability between minP and maxP
    p = minP + (maxP - minP) * rand();

    %% construct network with rewiring probability p and 
    %% calculate its neural complexity
    MF = Q2ConnectRun(p);    
    C = Q2NeuralComplexity(MF);

    %% save current probabiltiy and obtained complexity
    complexities(i) = C;
    probabilities(i) = p;    
  end

  figure(1)
  clf

  plot(probabilities, complexities, '.');
  xlabel('Rewiring probability p');
  xlim([0.1 0.5]);
  ylabel('Neural complexity');

  drawnow

end

