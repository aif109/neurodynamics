function MF = Q2ConnectRun(p)
  %%Q2CONNECTRUN 
  %% Generates a network with random rewiring probability (0.1 to 0.5) 
  %% and record the mean firing rates in 60 sec. runs for 'count' trials.
  
  Tmax = 60000; %% simulation time
  
  %% Generate and run network with rewiring probability p for Tmax ms
  [~, layer] = Q1Connect(p);
  
  [~, MF] = Q1Run(layer, Tmax);  
end

