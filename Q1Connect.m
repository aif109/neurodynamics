function [ConMatrix, layer] = Q1Connect(p)
  %% Generates a network with rewiring probability p and 
  %% plots the matrix connectivity.

  %% Construct two layers of Izhikevich neurons
  %% as modeled in the lecture notes
  N1 = 800;
  M1 = 1;
  N2 = 200;
  M2 = 1;

  totalModules = 8;
  totalPerModule = N1/totalModules;
  eeConnectionsPerModule = 1000;

  %% Layer 1 (excitatory neurons - regular spiking)
  layer{1}.rows = N1;
  layer{1}.columns = M1;
  %% assign parameters for Izhikevich neuron model
  r = rand(N1,M1);
  layer{1}.a = 0.02 * ones(N1,M1);
  layer{1}.b = 0.2 * ones(N1,M1);
  layer{1}.c = -65 + 15*r.^2;
  layer{1}.d = 8 - 6*r.^2;

  %% Layer 2 (inhibitory neurons - low threshold spiking)
  layer{2}.rows = N2;
  layer{2}.columns = M2;
  %% assign parameters for Izhikevich neuron model
  r = rand(N2,M2);
  layer{2}.a = 0.02 + 0.08*r;
  layer{2}.b = 0.25 - 0.05*r;
  layer{2}.c = -65 * ones(N2,M2);
  layer{2}.d = 2 * ones(N2,M2);

  %% Clear connectivity matrices
  L = length(layer);
  for i=1:L
    for j=1:L
      layer{i}.S{j} = [];
      layer{i}.factor{j} = [];
      layer{i}.delay{j} = [];
    end
  end

  %% Build connections as per lecture notes

  %% Excitatory-to-excitatory connections
  %% Projection pattern: modular small-world
  layer{1}.S{1} = zeros(N1*M1,N1*M1);
  for i=1:totalModules
    for j=1:eeConnectionsPerModule
      connectionNeurons = zeros(1,2);
      %% Select 2 neurons in the same module to connect;
      %% As per how the small-word network works, the neurons should not
      %% be already connected and should be distinct
      first = (i-1)*100+1;
      while connectionNeurons(1,1) == connectionNeurons(1,2) || ...
            layer{1}.S{1}( connectionNeurons(1,1), ...
			  connectionNeurons(1,2) ) == 1
        connectionNeurons = round( first + ...
				  (totalPerModule - 1) * rand(1,2) );
      end
      %% Connect the 2 neurons previously selected
      layer{1}.S{1}(connectionNeurons(1,1), connectionNeurons(1,2)) = 1;
    end
  end
  layer{1}.factor{1} = 17;
  layer{1}.delay{1} = round(20 * rand(N1*M1,N1*M1));

  %% Excitatory-to-inhibitory connections
  %% Projection pattern: focal from module
  layer{2}.S{1} = zeros(N2*M2,N1*M1);
  for i=1:N2    
    module = floor(totalModules * rand()) + 1;
    firstInModule = (module - 1) * 100 + 1;
    excNeurons = round(firstInModule + (totalPerModule - 1) * rand(1, 4));
    for j = 1 : 4
      layer{2}.S{1}(i, excNeurons(j)) = rand();
    end
  end
  layer{2}.factor{1} = 50;
  layer{2}.delay{1} = ones(N2*M2,N1*M1);

  %% Inhibitory-to-inhibitory connections
  %% Projection pattern: diffuse to whole network
  layer{2}.S{2} = -rand(N2*M2, N2*M2);
  layer{2}.factor{2} = 1;
  layer{2}.delay{2} = ones(N2*M2, N2*M2);

  %% Inhibitory-to-excitatory connections
  %% Projection pattern: diffuse to whole network
  layer{1}.S{2} = -rand(N1*M1,N2*M2);
  layer{1}.factor{2} = 2;
  layer{1}.delay{2} = ones(N1*M1,N2*M2);

  %% Rewiring
  for i=1:N1
    for j=1:N1
      if layer{1}.S{1}(i,j) > 0 && rand() < p
        currentModule = floor(i/100);
        if mod(i,100) ~= 0
          currentModule = currentModule + 1;
        end
        %% Pick a random module to connect to, which is not the
        %% same module as where the connection comes from
        randomModule = currentModule;
        while randomModule == currentModule
          randomModule =  floor(totalModules * rand()) + 1;
        end
        %% Pick a random node within that module
        randomNode = round((randomModule-1)*100 + 1 + ...
			   (totalPerModule - 1)*rand());
        %% Check if connection already exists from previous rewiring
        while layer{1}.S{1}(i,randomNode) > 0
          randomNode = round((randomModule-1)*100 + 1 + ...
                             (totalPerModule - 1)*rand());
        end
        %% Rewire
        layer{1}.S{1}(i,randomNode) = layer{1}.S{1}(i,j);
        layer{1}.S{1}(i,j) = 0;
      end
    end
  end

  %% get matrix connectivity
  A = horzcat(layer{1}.S{1}, layer{1}.S{2});
  B = horzcat(layer{2}.S{1}, layer{2}.S{2});
  ConMatrix = vertcat(A,B);
end

