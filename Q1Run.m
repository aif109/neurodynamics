function [layer, MF] = Q1Run(layer, Tmax) 
  N1 = layer{1}.rows;
  M1 = layer{1}.columns;

  N2 = layer{2}.rows;
  M2 = layer{2}.columns;

  Dmax = 50; %% maximum propagation delay
  MMax = 8; %% number of modules

  %% Initialise layers
  for lr=1:length(layer)
    layer{lr}.v = -65*ones(layer{lr}.rows,layer{lr}.columns);
    layer{lr}.u = layer{lr}.b.*layer{lr}.v;
    layer{lr}.firings = [];
  end


  %% Simulate

  for t = 1:Tmax    
    %% Remove ; in order to display time every 10ms
    if mod(t,10) == 0
      t;
    end
    
    %% Background firing generated by Poisson process at frequncy 1 Hz
    %% Achieved by giving the following base current to neurons
    lambda = 0.01;
    layer{1}.I = 15*(poissrnd(lambda,N1,M1) > 0);
    layer{2}.I = 15*(poissrnd(lambda,N2,M2) > 0);
    
    %% Update all the neurons
    for lr=1:length(layer)
      layer = IzNeuronUpdate(layer,lr,t,Dmax);
    end
  end

  firings1 = layer{1}.firings;
  
  %% Moving averages of firing rates in Hz for excitatory population
  ws = 50; %% window size
  ds = 20; %% slide window by 20 ms
  MF = zeros(MMax,ceil(Tmax/ds));
  
  %% MF(i,j) represents the firing rate for the ith module
  %% for the ws data points up to but not including j
  for i =1:MMax
    for j=1:ds:Tmax
      MF(i,ceil(j/ds)) = sum(firings1(:,1) >= (j-ws) &...
			     firings1(:,1) < j & firings1(:,2) > (i-1)*100 & ...
			     firings1(:,2) <= i*100) / (ws);
    end
  end
