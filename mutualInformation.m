function MI = mutualInformation(Xindex, S, ES)
  %%MUTUALINFORMATION 
  %% Calculate mutual information between X and S-{X}, 
  %% where X is at Xindex in system S, with system entropy ES

  d = size(S);
  N = d(1);

  %% get X
  X = S(Xindex, :);

  %% get S-{X} = Y
  if Xindex == 1
    Y = S(2:end, :);
  elseif Xindex == N
    Y = S(1:N-1, :);
  else
    Y = vertcat(S(1:Xindex-1, :), S(Xindex+1:end, :));
  end

  %% calculate mutual information
  MI = entropy(X) + entropy(Y) - ES;

end
