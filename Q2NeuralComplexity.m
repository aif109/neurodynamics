function C = Q2NeuralComplexity(MF)
  %%Q2NEURALCOMPLEXITY 
  %% Calculates neural complexity for a .mat file containing the time series
  %% data and rewiring probability.

  %% Discard first 50 points
  MF = MF(:, 51:end);

  %% Apply differencing twice
  S = aks_diff(aks_diff(MF));
    
  %% Whole system entropy
  ES = entropy(S);

  d = size(S);
  N = d(1);
  %% Calculate interaction complexity(approx. of neural complexity)
  SMI = 0;
  for i=1:N
    SMI = SMI + mutualInformation(i, S, ES);
  end
  C = SMI - integration(S, ES, N);

end

