function I = integration(S, ES, N)
  %%INTEGRATION 
  %% Calculate integration of system S with given system entropy ES
  SumEntropy = 0;
  for i =1:N
    SumEntropy = SumEntropy + entropy(S(i,:));
  end
  I = SumEntropy - ES;

end
