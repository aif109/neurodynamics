function H = entropy(S)
  %%ENTROPY 
  %% calculates entropy

  d = size(S);
  N = d(1);
  S = transpose(S); %% transpose S for the cov. matrix function

  %% Calculate entropy as per formula from lecture notes
  H = log( power(2*pi*exp(1), N) * det(cov(S)) ) / 2;

end

