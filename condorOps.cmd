universe = vanilla

executable = /homes/cac409/workspace/neurodynamics/runCondorSim.sh

output = condor/sim-$(Process).out
error  = condor/sim-$(Process).err

log = sim.log

arguments = -n -m -p

queue 50
