function Q1Main(p)
%% generates a network with rewiring probability p and runs it for 1s
  Tmax = 1000;
  
  [ConMatrix, layer] = Q1Connect(p);
  
  %% plot matrix connectivity
  figure(1)
  clf
  spy(ConMatrix);
  %% Uncomment to save matrix connectivity figure
  %% saveas(1,sprintf('question1/matrix-connectivity-%f.fig',p)) ;
  
  [layer, MF] = Q1Run(layer, Tmax);  
  
  %% PLots of neuron firings and mean firing rates
  firings1 = layer{1}.firings;
  
  figure(2)
  clf

  %% Raster plot of excitatory neurons firings
  N1 = layer{1}.rows;
  M1 = layer{1}.columns;

  subplot(2,1,1)
  if ~isempty(firings1)
    plot(firings1(:,1),firings1(:,2),'.')
  end
  xlim([0 Tmax])
  ylabel('Neuron number')
  ylim([0 N1*M1+1])
  set(gca,'YDir','reverse')
  title('Population 1 firings')

  %% Raster plot of mean firing rate
  subplot(2,1,2)
  plot(1:20:Tmax, MF)
  xlabel('Time (ms)');
  ylabel('Mean firing rate');

  drawnow

  %% Uncomment to save plot
  %% saveas(2,sprintf('question1/mfr-%f.fig',p));
end

